import sys
def check_construction(width, height, no_of_bricks_available, no_of_units_of_brick):
    construction = False
    no_of_bricks = no_of_bricks_available
    are_bricks_enough = ((no_of_bricks * no_of_units_of_brick) >= width*height)
    if are_bricks_enough:
        construction = True
    return construction
def get_the_brick(no_of_units_of_brick):
    length = no_of_units_of_brick
    a = no_of_units_of_brick * "*"
    b = "*" + (" " * (length - 2)) + "*"
    brick = (a + "\n" + b + "\n" + a)
    return brick

def print_the_wall(width,height, no_of_bricks_available, no_of_units_of_brick):
    for row in range(height):
        for j in range(3):
            if j == 0 or j == 2:
                print("*" * width)
            else:
                print(("*" + (" " * (no_of_units_of_brick - 2)) + "*") * (width // no_of_units_of_brick))

    print()





def main():
    one_unit = 5
    N1= one_unit * int(sys.argv[1])
    N2=int(sys.argv[2])
    N3 = int(sys.argv[3])
    N4 =int(sys.argv[4])
    N5 = int(sys.argv[5])
    no_of_units_of_N5 = one_unit
    no_of_units_of_N4 = (one_unit * 2)
    no_of_units_of_N3 = (one_unit * 3)
    type_3_brick = get_the_brick(no_of_units_of_N3)
    type_2_brick = get_the_brick(no_of_units_of_N4)
    type_1_brick = get_the_brick(no_of_units_of_N5)
    print("Type-3 brick")
    print(type_3_brick)
    print()
    print("Type-2 brick")
    print(type_2_brick)
    print()
    print("Type-1 brick")
    print(type_1_brick)
    for i in range(3):
        if i == 0:
            print("Type 1 Brick Wall")
            construction_type_1_bricks = check_construction(N1, N2, N5, no_of_units_of_N5)
            if construction_type_1_bricks:
                print_the_wall(N1, N2, N5, no_of_units_of_N5)
            else:
                print("CAN NOT BE DONE")
                print()
        if i == 1:
            print("Type 2 Brick Wall")
            construction_type_2_bricks = check_construction(N1, N2, N4, no_of_units_of_N4)
            if construction_type_2_bricks:
                print_the_wall(N1, N2, N4, no_of_units_of_N4)
            else:
                print("CAN NOT BE DONE")
                print()
        if i == 2:
            print("Type 3 Brick Wall")
            construction_type_3_bricks = check_construction(N1, N2, N3, no_of_units_of_N3)
            if construction_type_3_bricks:
                print_the_wall(N1, N2, N3, no_of_units_of_N3)
            else:
                print("CAN NOT BE DONE")
                print()

main()
